/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-21
	File:		Round.hpp
	Project:	BlackJackV2.0
	Brief:		Round class definition for preforming rounds of blackjack 
*/

#pragma once
#include <string>
#include "Player.hpp"
#include "Dealer.hpp"
#include <vector>
#include <memory>

class Round {
public:
	using pointer_type = std::shared_ptr<Round>;

private:
	enum Print_Selection {
		HOLE_CARD,
		NO_HOLE_CARD,
		RESULTS
	};

	static size_t count;

	void pre(PlayerList&, Dealer::pointer_type&);
	void post(PlayerList&, Dealer::pointer_type&);
	void print_routine(PlayerList const&, Dealer::pointer_type const&,Print_Selection const&);
public:
	Round() { ++count; }
	void start(PlayerList&, Dealer::pointer_type&);
};