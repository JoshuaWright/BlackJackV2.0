/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-19
	File:		Operations.hpp
	Project:	BlackJackV2.0
	Brief:		Operations hierarchy definition 
*/

#pragma once
#include <memory>
#include <string>


class Player;


#define DEF_OPERATION_EXE_DEF public: void execute(std::shared_ptr<Player>&, std::shared_ptr<Player>&);
#define DEF_OPERATION_EXE_IMP(T) void T::execute(std::shared_ptr<Player>& player, std::shared_ptr<Player>& player_dealer)

class Operation  {
public:
	using pointer_type = std::shared_ptr<Operation>;
	virtual void execute(std::shared_ptr<Player>&, std::shared_ptr<Player>&) = 0;
	virtual std::string to_string() const {
		return std::string(typeid(*this).name()).substr(6);
	}

	// Exceptions
	class XOperation : public std::exception {
	public: XOperation(std::string const& msg) : std::exception(msg.c_str()) {}
	};

	class XArg : public XOperation {
	public: XArg() : XOperation("Right argument must be a Dealer") {}
	};

	class XSplit : public XOperation {
	public: XSplit() : XOperation("Hand is not eligible for Split") {}
	};

	class XMutiSplit : public XOperation {
	public: XMutiSplit() : XOperation("Cannot Split Twice") {}
	};

};

class Hit			: public Operation { DEF_OPERATION_EXE_DEF };
class Stand			: public Operation {DEF_OPERATION_EXE_DEF };
class Split			: public Operation { DEF_OPERATION_EXE_DEF };
class Double_Down	: public Operation { DEF_OPERATION_EXE_DEF };
class Surrender		: public Operation { DEF_OPERATION_EXE_DEF };



class Result : public Operation , private std::enable_shared_from_this<Operation>{
public:
	using pointer_type = std::shared_ptr<Result>;
	Operation::pointer_type to_operation() { return shared_from_this(); }
};

class Win		: public Result	{ DEF_OPERATION_EXE_DEF };
class BlackJack : public Win	{};
class TwentyOne : public Win	{};
class Lose		: public Result { DEF_OPERATION_EXE_DEF };
class Bust		: public Lose	{};
class Push		: public Result { DEF_OPERATION_EXE_DEF };