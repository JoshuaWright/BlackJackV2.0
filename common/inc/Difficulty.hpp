/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		Difficulty.hpp
	Project:	BlackJackV2.0
	Brief:		Difficulty Interface definition and Implantation 
				Implements Bridge patter. 
				Used to Identify games difficulty and Load the dealers deck 
*/

#pragma once
//standard 
#include <memory>
#include <string>

#include "Dealer.hpp"

class Difficulty {
public: 
	using pointer_type = std::shared_ptr<Difficulty>;
	virtual void load(Dealer::deck_type&) = 0;
};

class Easy : public Difficulty {
public: virtual void load(Dealer::deck_type& deck) override { }
};

class Medium : public Difficulty {
public: virtual void load(Dealer::deck_type& deck) override {
	for (size_t i = 0; i < 2; ++i)
		deck += Dealer::deck_type::pointer_type(
			new Dealer::deck_type(
				Dealer::deck_pattern_type::pointer_type(
					new Dealer::deck_pattern_type)));
}
};

class Hard : public Difficulty {
public: virtual void load(Dealer::deck_type& deck) override {
	for (size_t i = 0; i < 4; ++i)
		deck += Dealer::deck_type::pointer_type(
			new Dealer::deck_type(
				Dealer::deck_pattern_type::pointer_type(
					new Dealer::deck_pattern_type)));
}
};

class Very_Hard : public Difficulty {
public: virtual void load(Dealer::deck_type& deck) override {
	for (size_t i = 0; i < 6; ++i)
		deck += Dealer::deck_type::pointer_type(
			new Dealer::deck_type(
				Dealer::deck_pattern_type::pointer_type(
					new Dealer::deck_pattern_type)));
}
};

class Unfair : public Difficulty {
public: virtual void load(Dealer::deck_type& deck) override {
	for (size_t i = 0; i < 8; ++i)
		deck += Dealer::deck_type::pointer_type(
			new Dealer::deck_type(
				Dealer::deck_pattern_type::pointer_type(
					new Dealer::deck_pattern_type)));
}
};