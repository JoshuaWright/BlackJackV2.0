/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-19
	File:		Result.hpp
	Project:	BlackJackV2.0
	Brief:		Result class to identify a hand's result
*/

#pragma once
#include <memory>
#include <string>
#include <sstream>



class Result {
public:
	using pointer_type = std::shared_ptr<Result>;
	Operation::pointer_type to_operation();
};

	class Win : public Result {};
		class BlackJack : public Win {};
		class TwentyOne : public Win {};

	class Lose : public Result {};
		class Bust : public Lose {};

	class Push : public Result {};