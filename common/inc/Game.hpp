/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		Game.hpp
	Project:	BlackJackV2.0
	Brief:		Game class definition 
*/

#pragma once
#include "Player.hpp"
#include "Dealer.hpp"
#include "Round.hpp"


class Game {
private:
	PlayerList				players_;
	Dealer::pointer_type	dealer_;

	void get_players();
	void get_difficulty();

public:
	Game() = default;

	void start();
};