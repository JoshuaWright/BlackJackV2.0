/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		Money.hpp
	Project:	BlackJackV2.0
	Brief:		Basic Money class used for all Money Operations 
*/

#pragma once
#include <string>
#include <locale>
#include <sstream>
#include <iomanip>

class Money {
public:
	using pointer_type	= std::shared_ptr<Money>;
	using value_type	= long double;
private:
	value_type value_;
public:
	Money(value_type value = 0 ): value_(value) {}
	std::string to_string() const {
		std::stringstream ss;
		ss.imbue(std::locale(""));
		ss << std::showbase << std::put_money(value_);
		
		return ss.str();
	}
	operator std::string() const { return to_string(); }
	operator value_type() { return value_; }
	value_type get_value() const { return value_; }
	void set_value(value_type const& value) { value_ = value; }
};