/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-18
	File:		Scanner.hpp
	Project:	BlackJackV2.0
	Brief:		Scanner class definition for collecting user input
*/

#pragma once
// Standard
#include <memory>
#include <map>

#include "Money.hpp"
#include <string>

// Forward declaration to avoid circular dependency
class Difficulty;
class Operation;

class Scanner {
private:
	std::map<std::string, std::shared_ptr<Difficulty>>	difficulty_dictionary_;
	std::map<std::string, std::shared_ptr<Operation>>	decisions_dictionary_;

public:
	Scanner();
	int							get_int();
	bool						get_bool();
	Money::pointer_type			get_money();
	std::shared_ptr<Difficulty>	get_difficulty();
	std::shared_ptr<Operation > get_operation();
};

