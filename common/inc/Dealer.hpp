/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		Dealer.hpp
	Project:	BlackJackV2.0
	Brief:		Dealer class definition. 
				Derived from Player 
*/

#pragma once
#include "Player.hpp"

// Forward declaration to avoid circular dependency
class Difficulty;

class Dealer : public Player {
public:
	using pointer_type		= std::shared_ptr<Dealer>;
	using deck_type			= PlayingCards::French::French_Deck;
	using deck_pattern_type = PlayingCards::French::English_Pattern;
private:
	deck_type deck_;
public:
	Dealer(std::shared_ptr<Difficulty>&);

	void								shuffle();
	void								deal(Hand::pointer_type&);
	Hand::card_pointer_type				deal();
	void								result(Player::pointer_type&);
	void								pick_up(Hand::pointer_type&);
	void								pick_up();
	void								S17();
};