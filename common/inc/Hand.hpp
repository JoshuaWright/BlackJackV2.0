/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-18
	File:		Hand.hpp
	Project:	BlackJackV2.0
	Brief:		Hand class definition to be used throughout project 
*/

#pragma once
// standard 
#include <memory>
#include <vector>

// PlayingCards library
#include <PlayingCards.hpp>

#include "Money.hpp"
#include "Operations.hpp"
#define BLACKJACK 21

// Forward declaration to avoid circular dependency
class Dealer;

class Hand {
public:
	using pointer_type		= std::shared_ptr<Hand>;
	using value_type		= PlayingCards::Deck::value_type;
	using card_pointer_type	= PlayingCards::Card::pointer_type;
	using card_type			= PlayingCards::Regular_Card;

private:
	value_type				value_;
	Money::pointer_type		bet_;
	Result::pointer_type	result_;
	bool					isPlaying_;

public:
	Hand() : isPlaying_(true) {}

	bool						is_bust() const;
	bool						has_blackjack() const;
	size_t						total() const;
	Hand::pointer_type			split();
	Money::pointer_type			bet() const;
	void						bet(Money::pointer_type const&);
	void						result(Result::pointer_type const& result) { result_ = result; }
	Result::pointer_type		result() const { return result_; }
	bool						isPlaying() const { return isPlaying_; }
	void						isPlaying(bool const& value) { isPlaying_ = value; }

	value_type::size_type		size() const { return value_.size(); }
	card_type					at(size_t const&) const;	
	card_pointer_type			back() const { return value_.back(); }
	value_type::iterator		begin() { return value_.begin(); }
	value_type::iterator		end() { return value_.end(); }
	value_type::const_iterator	cbegin() { return value_.cbegin(); }
	value_type::const_iterator	cend() { return value_.cend(); }
	void						operator+= (card_pointer_type const&);
	card_pointer_type			operator--();


	// Exceptions
	class XHand : public std::exception {
	public: XHand(std::string const& msg) : std::exception(msg.c_str()) {}
	};

	class XCannotSplit : public XHand {
	public: XCannotSplit() : XHand("Hand must have 2 Cards to Split") {}
	};

	class XInvalidBet : public XHand {
	public: XInvalidBet() : XHand("Bet value is Invalid.") {}
	};

	class XNoBet : public XHand {
	public: XNoBet() : XHand("Bet must be Assigned a value.") {}
	};

};


typedef std::vector<Hand::pointer_type> HandList;