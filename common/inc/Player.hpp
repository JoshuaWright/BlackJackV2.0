/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		PLayer.hpp
	Project:	BlackJackV2.0
	Brief:		Player class definition. 
*/

#pragma once

// standard 
#include <string>
#include <memory>

#include "Hand.hpp"
#include "Money.hpp"
#include "Operations.hpp"

// Forward declaration to avoid circular dependency
//class Operation;

/*
	Derivation from  std::enable_shared_from_this<Player>
	To easy be able to return a std::shared_ptr<Player> form (this)
	using the shared_from_this() function.
*/
class Player : public std::enable_shared_from_this<Player> {
public:
	using pointer_type = std::shared_ptr<Player>;
	friend class Operation;

private:
	static size_t			count;
	size_t					number_;
	bool					isBankrupt_;
	Money::pointer_type		bank_;
	size_t					active_hand_;

protected:
	HandList handlist_;

public:
	Player(Money::pointer_type const&);

	Hand::pointer_type	hand() { return handlist_[active_hand_]; }
	size_t				number() const { return number_; }
	void				execute(std::shared_ptr<Operation>&, pointer_type);	
	void				reset();

	Money::pointer_type bet(size_t index = 0) { return hand()->bet(); }
	void				bet(Money::pointer_type const&,size_t const& index = 0);
	Money::pointer_type bank(size_t index = 0) { return bank_; }
	void				add_to_bank(Money::pointer_type const& value);
	void				take_from_bank(Money::pointer_type const&);

	bool				isPlaying() const;
	HandList			handlist() const { return handlist_; }

	void				active_hand(size_t const& value);
	size_t				active_hand() const { return active_hand_; }
	size_t				split_count() const { return handlist_.size() - 1; }
	void				operator+=(Hand::pointer_type& hand) { handlist_.push_back(hand); }
	
	virtual ~Player() = default;


	// Exceptions
	class XPlayer : public std::exception {
	public: XPlayer(std::string const& msg) : std::exception(msg.c_str()) {}
	};

	class XInvalidBet : public XPlayer {
	public: XInvalidBet() : XPlayer("Bet must be GREATER than 0 and LESS than Buy in.") {}
	};

	class XInvalidBank : public XPlayer {
	public: XInvalidBank() : XPlayer("Bank value is Invalid.") {}
	};

	class XInvalidIndex : public XPlayer {
	public: XInvalidIndex() : XPlayer("Hand dose not Exist at that index.") {}
	};
};


using PlayerList = std::vector<Player::pointer_type>;

/*
	Purpose: To Find any players in a PlayerList are still playing 
*/
inline bool isPlaying(PlayerList const& list) {
	size_t count = list.size();
	for each (auto player in list)
		if (!player->isPlaying())
			--count;
	
	if (count == 0)
		return false;
	return true;
}