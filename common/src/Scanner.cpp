/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-18
	File:		Scanner.cpp
	Project:	BlackJackV2.0
	Brief:		Scanner class implantation for collecting user input
*/

//Standard 
#include <iostream>
#include <regex>

#include "Scanner.hpp"
#include "Operations.hpp"
#include "Difficulty.hpp"
using namespace std;


Scanner::Scanner() {
	difficulty_dictionary_[" *easy *"]			= Difficulty::pointer_type(new Easy);
	difficulty_dictionary_[" *medium *"]		= Difficulty::pointer_type(new Medium);
	difficulty_dictionary_[" *hard *"]			= Difficulty::pointer_type(new Hard);
	difficulty_dictionary_[" *very *hard *"]	= Difficulty::pointer_type(new Very_Hard);
	difficulty_dictionary_[" *unfair *"]		= Difficulty::pointer_type(new Unfair);

	decisions_dictionary_[" *hit *"]				= Operation::pointer_type(new Hit);
	decisions_dictionary_[" *stand *| *hold *"]		= Operation::pointer_type(new Stand);
	decisions_dictionary_[" *split *"]				= Operation::pointer_type(new Split);
	decisions_dictionary_[" *surrender *"]			= Operation::pointer_type(new Surrender);
	decisions_dictionary_[" *double *down *"]		= Operation::pointer_type(new Double_Down);
}


int Scanner::get_int() {
	int input = 0;
	do {
		cin >> input;
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	} while (cin.fail());
	return input;
}

bool Scanner::get_bool() {
	string input;
	regex t("yes", regex::icase);
	regex f("no", regex::icase);
	for (;;) {
		do {
			cin >> input;
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		} while (cin.bad());

		if (regex_match(input, t))
			return true;
		if (regex_match(input, f))
			return false;
		cout << "yes or no: ";
	}
}

Money::pointer_type Scanner::get_money() {
	double input = 0;
	do {
		cin >> input;
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	} while (cin.bad());
	return Money::pointer_type(new Money(input));
}

Difficulty::pointer_type Scanner::get_difficulty() {
	string input;
	regex r;
	for (;;) {
		do {
			cin >> input;
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		} while (cin.fail());

		for (auto it = difficulty_dictionary_.begin(); it != difficulty_dictionary_.end(); ++it) {
			r.assign(it->first, regex::icase);
			if (regex_match(input, r))
				return it->second;
		}
		cout << "Enter a Difficulty level listed above: ";
	}// end for 
}

Operation::pointer_type Scanner::get_operation() {
	string input;
	regex r;
	for (;;) {
		do {
			cin >> input;
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		} while (cin.fail());

		for (auto it = decisions_dictionary_.begin(); it != decisions_dictionary_.end(); ++it) {
			r.assign(it->first, regex::icase);
			if (regex_match(input, r))
				return it->second;
		}
		cout << "Enter a Option listed above: ";
	}// end for 
}