/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-21
	File:		ut_Table.cpp
	Project:	BlackJackV2.0
	Brief:		Table class Unit Test
*/

// Boost
#define BOOST_TEST_MODULE Table_ut
#include <boost\test\unit_test.hpp>

// Standard
#include <iostream>
#include <sstream>

#include "Table.hpp"
using namespace std;


BOOST_AUTO_TEST_CASE(Intro) {
	cout << endl << endl << "Table Unit Test" << endl
		<< "Last Build:	" << __TIMESTAMP__ << endl << endl << endl;
}

BOOST_AUTO_TEST_CASE(tow_by_two) {
	cout << "2x2:";
	table t(2,2);
	t.at(0, 0) = "Header 1";
	t.at(1, 0) = "Header 2";
	t.at(0, 1) = "Data 1";
	t.at(1, 1) = "Data 2";
	t.print();
	cout << endl << endl;
}

BOOST_AUTO_TEST_CASE(functionss) {
	table t;
	stringstream ss;

	t.push_column();
	t.push_row();
	t.at(columns::A, 0) = "0,0 ";
	cout << "1x1:" << t.to_string() << endl << endl;

	
	for (size_t col = 1; col < 5; ++col) {
		t.push_column();
		t.push_row();
	}
	for (size_t col = 1; col < 5; ++col) {
		for (size_t row = 1; row < 5; ++row) {
			ss << col << "," << row << " ";
			t.at(col, row) = ss.str();
			ss.str("");
		}
	}

	cout << "5x5:";
	t.print();
}