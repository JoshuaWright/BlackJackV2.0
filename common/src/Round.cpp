/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-21
	File:		Round.hpp
	Project:	BlackJackV2.0
	Brief:		Round class implementation for preforming rounds of blackjack 
*/

// standard
#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <sstream>

// output_table lib
#include <output_table.hpp>

#include "Round.hpp"
#include "Scanner.hpp"

using namespace std;

size_t Round::count = 0;


void Round::pre(PlayerList& players, Dealer::pointer_type& dealer) {
	cout << endl << "------------------ Round " << count << " ------------------" << endl << endl;

	Scanner scanner;

	// Ask For Bets
	for (auto it = players.begin(); it != players.end(); ++it) {
		for (;;) {
			try {
				cout << "Player " << it->get()->number() << " place bet: ";
				it->get()->bet(scanner.get_money());
				break;
			}
			catch (std::exception& e) {
				cout << e.what() << endl;
			}
		}
	}

	// Deal Cards
	dealer->shuffle();
	dealer->deal(dealer->hand());
	for (auto it = players.begin(); it != players.end(); ++it) {
		dealer->deal(it->get()->hand());
	}
}

void Round::post(PlayerList& players, Dealer::pointer_type& dealer) {
	stringstream ss;
	Scanner scanner;

	for each (auto player in players)
		dealer->result(player);

	print_routine(players, dealer, NO_HOLE_CARD);

	for each (auto player in players) {
		if (player->split_count() > 0) 
			for (size_t i = 0; i <= player->split_count(); ++i) {
				player->active_hand(i);
				player->execute(player->hand()->result()->to_operation(), dealer);
			}
		else
			player->execute(player->hand()->result()->to_operation(), dealer);
	}
	dealer->pick_up();

	print_routine(players, dealer, RESULTS);

	for (auto player : players) 
		player->reset();
	

	cout << "Continue Playing?" << endl;
	for (auto it = players.begin(); it != players.end();) {
		cout << "PLayer " << it->get()->number() << ": ";
		if (!scanner.get_bool())
			it = players.erase(it);
		else
			++it;
	}
}

void Round::print_routine(	PlayerList const& players, 
							Dealer::pointer_type const& dealer, 
							Print_Selection const& print_selection) 
{
	stringstream ss;
	size_t row_number = 1;
	size_t hand_number;
	
	if (print_selection != RESULTS)
	{
		output_table t(players.size() + 1,4);
		
		t.at(0, 0) = "Dealer			";
		t.at(0, 1) = "----------		";

		for (auto player : players) {
			ss.str("");
			ss << "Player " << player->number() << "		";

			while (player->number() >= t.column_size())
				t.push_column();

			t.at(player->number(), 0) = ss.str();
			t.at(player->number(), 1) = "----------		";
		}

		size_t hole_card = 0;
		if (print_selection == HOLE_CARD) 
		{
			t.at(0,++row_number) = "HOLE CARD		";
			++hole_card;
		}

		for (auto it = dealer->hand()->cbegin() + hole_card; it != dealer->hand()->cend(); ++it) {
			if (row_number + 1 >= t.row_size()) 
				t.push_row();		
			t.at(0, ++row_number) = it->get()->to_string() + "		";
		}
	
		for (auto player : players) {
			row_number = 1;
			hand_number = -1;
			if (player->split_count() > 0) 
			{
				for (auto hand : player->handlist()) {
					player->active_hand(++hand_number);
					ss.str("");
					ss << "Hand " << player->active_hand() + 1 << "			";

					if (row_number + 1 >= t.row_size()) 
						t.push_row();					
					t.at(player->number(), ++row_number) = ss.str();

					if (row_number + 1 >= t.row_size()) 
						t.push_row();			
					t.at(player->number(), ++row_number) =  "^^^^^^^^^^		";

					for (auto card : *player->hand()) {
						if (row_number + 1 >= t.row_size())
							t.push_row();
						t.at(player->number(), ++row_number) = card->to_string() + "		";
					}
					if (row_number + 1 >= t.row_size())
						t.push_row();
					t.at(player->number() - 1, ++row_number) = "";
				}
			}
			else 
			{
				for (auto card : *player->hand()) {
					if (row_number + 1 >= t.row_size())
						t.push_row();
					t.at(player->number(), ++row_number) = card->to_string() + "		";
				}
			}

		}
	
		t.expand();
		t.replace("", "			");
		t.print();	
	}
	else
	{
		output_table t(players.size(),2);

		for (auto player : players) {
			ss.str("");
			ss << "Player " << player->number() << "		";

			while (player->number() - 1 >= t.column_size())
				t.push_column();

			t.at(player->number() - 1, 0) = ss.str();
			t.at(player->number() - 1, 1) = "----------		";
		}


		for (auto player : players) {

			row_number = 1;
			hand_number = -1;
			if (player->split_count() > 0)
			{
				for (auto hand : player->handlist()) {
					player->active_hand(++hand_number);
					ss.str("");
					ss << "Hand " << player->active_hand() << "			";

					if (row_number + 1 >= t.row_size())
						t.push_row();
					t.at(player->number() - 1, ++row_number) = ss.str();

					if (row_number + 1 >= t.row_size())
						t.push_row();
					t.at(player->number() - 1, ++row_number) = "^^^^^^^^^^		";
				
					if (row_number + 1 >= t.row_size())
						t.push_row();
					ss.str("");
					ss << player->hand()->result()->to_string()
						<< " - " << player->hand()->bet()->to_string()
						<< "		";
					t.at(player->number() - 1, ++row_number) = ss.str();

					if (row_number + 1 >= t.row_size())
						t.push_row();
					t.at(player->number() - 1, ++row_number) = "";
				}
			}
			else
			{
				if (row_number + 1 >= t.row_size())
					t.push_row();
				ss.str("");
				ss << player->hand()->result()->to_string() 
					<< " - " << player->hand()->bet()->to_string()
					<< "		";
				t.at(player->number() - 1, ++row_number) = ss.str();
			}

			if (row_number + 1 >= t.row_size())
				t.push_row();
			ss.str("");
			ss << "Bank - " << player->bank()->to_string() << "		";
			t.at(player->number() - 1, ++row_number) = ss.str();

		}
		t.expand();
		t.replace("", "			");
		t.print();
	}
	cout << endl;
}


void Round::start(PlayerList& players, Dealer::pointer_type& dealer) {
	Scanner scanner;

	pre(players, dealer);

	while (isPlaying(players)) {

		print_routine(players, dealer, HOLE_CARD);

		dealer->S17();
		cout << "Hit, Stand, Split, Double Down or Surrender?" << endl;

		for each (Player::pointer_type player in players)
			if (player->isPlaying()) {
				if (player->split_count() > 0) {
					for (size_t i = 0; i <= player->split_count(); ++i) {
						player->active_hand(i);
						if (player->hand()->isPlaying()) {
							for (;;) {
								try {
									cout << "Player " << player->number() << " hand " << i + 1 << ": ";
									player->execute(scanner.get_operation(), dealer);
									break;
								}
								catch (exception& e) {
									cout << e.what() << endl;
								}
							}// end inner for 
						}
					} // end outer for
				}
				else {
					for (;;) {
						try {
							cout << "Player " << player->number()  << ": ";
							player->execute(scanner.get_operation(), dealer);
							break;
						}
						catch (exception& e) {
							cout << e.what() << endl;
						}
					}// for 	
				}
			}
		cout << endl;
		} // while

	post(players, dealer);
}