/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-19
	File:		Operations.cpp
	Project:	BlackJackV2.0
	Brief:		Operations hierarchy implication 
*/

#include "Operations.hpp"
#include "Player.hpp"
#include "Dealer.hpp"
using namespace std;


#define DEF_NO_DEALER_THROW if(!dynamic_pointer_cast<Dealer>(player_dealer)) throw XArg();
#define DEF_CONVERT_DEALER auto dealer = dynamic_pointer_cast<Dealer>(player_dealer);


DEF_OPERATION_EXE_IMP(Hit) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	*player->hand() += dealer->deal();
	if (player->hand()->is_bust() || player->hand()->has_blackjack())
		player->hand()->isPlaying(false);
}

DEF_OPERATION_EXE_IMP(Stand) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	player->hand()->isPlaying(false);
}

DEF_OPERATION_EXE_IMP(Split) {
	DEF_NO_DEALER_THROW
		DEF_CONVERT_DEALER

	if (player->hand()->size() < 2)
		throw XSplit();
	if (player->split_count() == 1)
		throw XMutiSplit();

	// TODO
	auto rank1 = player->hand()->at(0).get_rank();
	auto rank2 = player->hand()->at(1).get_rank();

	if ( 
		(rank1 < 9 && rank1 != 1) || 
		(rank2 < 9 && rank2 != 1) &&
		(rank1 != rank2)
		)
		throw XSplit();

	*player += player->hand()->split();

	auto bet = player->hand()->bet();
	player->active_hand(player->active_hand() + 1);
	player->hand()->bet(bet);
}

DEF_OPERATION_EXE_IMP(Double_Down) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	*player->hand() += dealer->deal();
	*player->bet(*player->bet() * 2);
	player->hand()->isPlaying(false);
}


DEF_OPERATION_EXE_IMP(Surrender) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	*player->bet(*player->bet() * 0.5);
	player->hand()->isPlaying(false);
}


// Result

DEF_OPERATION_EXE_IMP(Win) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	player->add_to_bank(player->bet());

	for (auto hand : player->handlist())
		dealer->pick_up(hand);

	player->hand()->isPlaying(true);
}

DEF_OPERATION_EXE_IMP(Push) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	for (auto hand : player->handlist())
		dealer->pick_up(hand);

	player->hand()->isPlaying(true);
}

DEF_OPERATION_EXE_IMP(Lose) {
	DEF_NO_DEALER_THROW
	DEF_CONVERT_DEALER

	player->take_from_bank(player->bet());

	for (auto hand : player->handlist())
		dealer->pick_up(hand);

	player->hand()->isPlaying(true);
}
