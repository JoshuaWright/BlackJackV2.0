/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		Game.cpp
	Project:	BlackJackV2.0
	Brief:		Game class implantation 
*/

#include "Game.hpp"
#include <iostream>
#include "Scanner.hpp"
using namespace std;


void Game::get_players() {
	Scanner scanner;
	int number_of_players = 0;
	Money::pointer_type buy_in;

	do {
		cout << "Enter the Number of Players ( 1 - 4 ) : ";
		number_of_players = scanner.get_int();
	} while (number_of_players > 4 || number_of_players < 1);

	for (int i = 1; i <= number_of_players; ++i) {	
		for (;;) {
			try {
				cout << "Enter buy-in for Player " << i << ": ";
				buy_in = scanner.get_money();		
				players_.push_back(Player::pointer_type(new Player(buy_in)));
				break;
			}
			catch (exception& e) {
				cout << e.what() << endl;
			}
		} // end for 
	}// end for
	cout << endl;
}

void  Game::get_difficulty() {
	cout << "Level Of Difficulty:"<< endl;
	cout << "Easy		( 1 Deck   )"<< endl;
	cout << "Medium		( 2 Deck's )"<< endl;
	cout << "Hard		( 4 Deck's )"<< endl;
	cout << "Very Hard	( 6 Deck's )"<< endl;
	cout << "Unfair		( 8 Deck's )"<< endl << endl;
	cout << "Enter Difficulty: ";

	Scanner scanner;
	dealer_ = Dealer::pointer_type(new Dealer(scanner.get_difficulty()));
	cout << endl;
}

void  Game::start() {
	get_difficulty();
	get_players();

	for (; players_.size() > 0;) {
		Round round;
		round.start(players_,dealer_);
	}
 }