/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		Dealer.cpp
	Project:	BlackJackV2.0
	Brief:		Dealer class implantation 
*/

#include "Dealer.hpp"
#include "Difficulty.hpp"
#include <algorithm>
using namespace std;
using namespace PlayingCards;


Dealer::Dealer(Difficulty::pointer_type& difficulty) : 
	Player(Money::pointer_type(new Money(0)))
	, deck_(Deck_Pattern::pointer_type(new deck_pattern_type))  {
	difficulty->load(deck_);
}

void Dealer::shuffle() {
	srand(static_cast<size_t>(time(0)));
	random_shuffle(deck_.begin(), deck_.end());
}

void Dealer::deal(Hand::pointer_type& hand) {
	hand->operator+=(--deck_);
	hand->operator+=(--deck_);
}

Hand::card_pointer_type Dealer::deal() {
	return --deck_;
}

void Dealer::pick_up(Hand::pointer_type& hand) {
	while (hand->size() > 0) {
		deck_ += hand->operator--();
	}
	hand->isPlaying(true);
}

void Dealer::result(Player::pointer_type& player) {
	for each (auto hand in player->handlist()) {
		if (hand->has_blackjack())
			hand->result(Result::pointer_type(new BlackJack));
		else if (hand->is_bust() && this->hand()->is_bust())
			hand->result(Result::pointer_type(new Push));
		else if (hand->is_bust())
			hand->result(Result::pointer_type(new Lose));
		else if ( ( hand->total() > this->hand()->total())
			&& !this->hand()->is_bust())
			hand->result(Result::pointer_type(new Win));
		else if (hand->total() < this->hand()->total()
			&& !this->hand()->is_bust())
			hand->result(Result::pointer_type(new Lose));
		else
			hand->result(Result::pointer_type(new Push));
	}
}

void Dealer::S17() {
	if (hand()->total() < 17) 
		*hand() += deal();
}

void Dealer::pick_up() {
	while (hand()->size() > 0) {
		deck_ += hand()->operator--();
	}
}