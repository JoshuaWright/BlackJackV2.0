/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-18
	File:		Hand.cpp
	Project:	BlackJackV2.0
	Brief:		Hand class implantation to be used throughout project 
*/

#include "Hand.hpp"
#include "Dealer.hpp"
using namespace std;
using namespace PlayingCards;


size_t Hand::total() const {
	size_t total = 0;
	for each(auto card in value_) {
		if (auto cp = dynamic_pointer_cast<Regular_Card>(card))
			if (cp->get_rank() > 10)
				total += 10;
			else
				total += cp->get_rank();
	}
	for each(auto card in value_) {
		if (auto cp = dynamic_pointer_cast<Regular_Card>(card))
			if (cp->get_rank() == 1 && (total + 10 <= BLACKJACK))
				total += 10;
	}

	return total;
}

bool Hand::has_blackjack() const {
	if (total() == BLACKJACK)
		return true;
	return false;
}

bool Hand::is_bust() const {
	if (total() > BLACKJACK)
		return true;
	return false;
}

Hand::pointer_type Hand::split() {
	if (size() != 2)
		throw XCannotSplit();
	Hand::pointer_type hand(new Hand());
	*hand += this->operator--();
	return hand;
}


Hand::card_type Hand::at(size_t const& index) const {
	return *dynamic_pointer_cast<Regular_Card>(value_.at(index));
}

void Hand::operator+=( Hand::card_pointer_type const& card) {
	value_.push_back(card);
}

Hand::card_pointer_type Hand::operator--() {
	auto result = value_.back();
	value_.pop_back();
	return result;
}

Money::pointer_type Hand::bet() const {
	if (bet_ == nullptr)
		throw XNoBet();
	return bet_;
}

void Hand::bet(Money::pointer_type const& money) {
	if (*money <= 0)
		throw XInvalidBet();

	bet_ = money;
}

