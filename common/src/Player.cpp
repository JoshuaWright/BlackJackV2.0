/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-12
	File:		PLayer.cpp
	Project:	BlackJackV2.0
	Brief:		Player class implantation 
*/

#include "Player.hpp"
#include "Dealer.hpp"
#include "Operations.hpp"
using namespace std;

size_t Player::count = 0;

Player::Player(Money::pointer_type const& bank) : number_(count++), active_hand_(0) {
	if (bank->get_value() < 0)
		throw XInvalidBank();

	bank_ = bank;
	handlist_.push_back(Hand::pointer_type(new Hand));
}

void Player::execute(std::shared_ptr<Operation>& operation, pointer_type dealer) {
	if (!dynamic_pointer_cast<Dealer>(dealer))
		throw Operation::XArg();
	
	operation->execute(shared_from_this(), dealer);
}

void Player::bet(Money::pointer_type const& value, size_t const& index) {
	if (*value <= 0)
		throw XInvalidBet();
	if(*value > *bank_)
		throw XInvalidBet();
	if (index >= handlist_.size())
		throw XInvalidIndex();

	handlist_[index]->bet(value);
}

void Player::add_to_bank(Money::pointer_type const& money) {
	bank_->set_value(bank_->get_value() + money->get_value()); 
}

void Player::take_from_bank(Money::pointer_type const& money) {
	if (money->get_value() > bank_->get_value())
		throw XInvalidBank();

	bank_->set_value(bank_->get_value() - money->get_value());

	if (bank_->get_value() == 0)
		isBankrupt_ = true;
}

void Player::active_hand(size_t const& index) {
	if (index >= handlist_.size())
		throw XInvalidIndex();

	active_hand_ = index;
}

bool Player::isPlaying() const {
	size_t count = handlist_.size();

	for each (auto hand in handlist_) 
		if (!hand->isPlaying())
			--count;
	
	if (count == 0)
		return false;
	return true;
}


void Player::reset() {
	while (handlist_.size() > 1)
		handlist_.pop_back();

	active_hand_ = 0;
}