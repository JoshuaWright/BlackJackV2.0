/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-15
	File:		main_BlackJac.cpp
	Project:	BlackJackV2.0
	Brief:		BlackJac main entry point 
*/

// Standard 
#include <iostream>

#include "Game.hpp"

int main() {
	std::cout << "BlackJack v2.0, (c) 2016 Joshua Wright" << std::endl << std::endl;
	Game blackjack;
	blackjack.start();
}