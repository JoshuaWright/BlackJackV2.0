/*	Author:		Joshua Wright, joshuawright1197@gmail.com
	Date:		2017-01-14
	File:		ut_BlackJac.cpp
	Project:	BlackJackV2.0
	Brief:		BlackJac Unit Test
*/

// Boost
#define BOOST_TEST_MODULE BlackJack_ut
#include <boost\test\unit_test.hpp>

// Standard
#include <iostream>

#include "Game.hpp"
using namespace std;


BOOST_AUTO_TEST_CASE(Intro) {
	cout << endl << endl << "BlackJack V2.0 game Unit Test" << endl
		<< "Last Build:	" << __TIMESTAMP__ << endl << endl << endl;
}

BOOST_AUTO_TEST_CASE(ut_main) {
	Game blackjack;
	blackjack.start();
}